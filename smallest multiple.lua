function findSmallestMultiple()
  
  found = false
  
  smallestMultiple = 1
  
  currentTest = 0
  
  while (smallestMultiple == 1) do
    
    if (currentTest%100000 == 0) then
      print("Current Test: " .. currentTest)
    end
    
    if (isDisviableByAll(currentTest)) then
      smallestMultiple = currentTest
    end
    
    currentTest = currentTest + 20
    
  end
  
  
  return smallestMultiple
  
end

function isDisviableByAll(gNum)
  disviableByAll = true
  
  if (gNum == 0) then
    return false
  end
  
  i = 1
  for i=1, 20, 1 do
    if (gNum%i > 0) then
      disviableByAll = false
    end
  end
  
  return disviableByAll
  
end

print(findSmallestMultiple())